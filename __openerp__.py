# -*- coding: utf-8 -*-
##############################################################################
#
#    
#
##############################################################################


{
    'name': 'Routing',
    'version': '1.0',
    'category': 'Inventory/stock',
    'sequence': 6,
    'summary': 'Add Routing Information',
    'description': """

=======================

This module adds the following features:
    * 

""",
    'author': 'Charles Maina',
    'depends': ['base','stock','account'],
    'data': [

        'routing_view.xml',
    ],
    'installable': True,
    'website': 'https://www.copiakenya.com/',
    'auto_install': False,
    'qweb': [
		#'views/.xml',
    ],
}
